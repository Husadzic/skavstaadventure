import sv from './sv/index'
import en from './en/index'


export default {
	default : 'en',
	en: en,
	sv: sv
}